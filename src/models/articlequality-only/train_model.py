import json
import numpy as np
import pandas as pd

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

ores_scores = []
with open('../../../data/interim/ores-scores.json', 'r') as f:
    for line in f:
        ores_scores.append(json.loads(line))
df_ores_scores = pd.DataFrame(ores_scores)
df_ores_scores = df_ores_scores[df_ores_scores.error.isnull()]
df_ores_scores = df_ores_scores.drop('error', axis=1)

df_judgements = pd.read_csv('../../../data/interim/annotated-judgements.csv').drop('Unnamed: 0', axis=1)
df_merged = pd.merge(df_judgements, df_ores_scores, on='revid')

Xs = np.array(df_merged[['Stub', 'Start', 'C', 'B', 'GA', 'FA']])
Ys = np.array(df_merged['judgement'])

Xs_train, Xs_test, Ys_train, Ys_test = train_test_split(Xs, Ys, test_size=0.2)

model = LogisticRegression().fit(Xs_train, Ys_train)
Ys_pred = model.predict(Xs_test)
print(accuracy_score(Ys_test, Ys_pred))