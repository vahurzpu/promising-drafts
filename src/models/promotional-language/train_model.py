import fasttext

model = fasttext.train_supervised('../../../data/interim/promo-tags/train.txt', epoch=50, lr=0.3)
model.save_model("../../../models/promotional/fasttext.bin")