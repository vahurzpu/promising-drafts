import json
import pandas as pd

all_records = []
with open('../../data/processed/features.jsonl', 'r') as f:
    for line in f:
        all_records.append(json.loads(line))
pd.DataFrame(all_records).to_csv('../../data/processed/features.csv')
