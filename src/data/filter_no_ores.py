import json
import pandas as pd

ores_scores = []
with open('../../data/interim/ores-scores.json', 'r') as f:
    for line in f:
        ores_scores.append(json.loads(line))
df_ores_scores = pd.DataFrame(ores_scores)
df_ores_scores = df_ores_scores[df_ores_scores.error.isnull()]
df_ores_scores = df_ores_scores.drop('error', axis=1)

df_judgements = pd.read_csv('../../data/interim/annotated-judgements.csv').drop('Unnamed: 0', axis=1)
df_judgements = df_judgements[df_judgements.revid.isin(df_ores_scores.revid)]
df_judgements.to_csv('../../data/interim/filtered-judgements.csv', index=False)