from re import sub
from typing import List
import requests
import pywikibot
from xml.etree import ElementTree as ET
from tqdm import tqdm
from pathlib import Path

USER_AGENT = "DraftAcceptPrediction/0.0 (by User:Vahurzpu; mmwootten@outlook.com)"

enwiki = pywikibot.Site('en', 'wikipedia')
declined_submissions = pywikibot.Category(enwiki, 'Category:Declined AfC submissions').articles()
declined_submissions: List[pywikibot.Page] = [page for page in declined_submissions if page.namespace() != enwiki.namespace(14)]
for submission in tqdm(declined_submissions):
    page_xml_file = Path(f'../../data/raw/declined-drafts/{submission.pageid}.xml')
    if page_xml_file.exists():
        continue
    try:
        exported_xml = requests.get('https://en.wikipedia.org/wiki/Special:Export', params={
            'pages': submission.title(),
            'history': 1
        }, headers={
            'User-Agent': USER_AGENT
        }).text
        ns = {'mw': 'http://www.mediawiki.org/xml/export-0.10/'}
        page_id = int(ET.fromstring(exported_xml).find('mw:page', ns).find('mw:id', ns).text)
        assert page_id == submission.pageid
        with open(page_xml_file, 'w') as f:
            f.write(exported_xml)
    except Exception as e:
        print(submission.pageid, submission.title())
        print(e)
