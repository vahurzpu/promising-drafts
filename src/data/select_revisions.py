from pathlib import Path
from xml.etree import ElementTree as ET
import pandas as pd
from tqdm import tqdm
import json

accepted_folder = Path('../../data/raw/accepted-drafts')
declined_folder = Path('../../data/raw/declined-drafts')

# Goal of the algorithm: get a bunch of revision IDs at which a judgement was made.
# Basically: what was 

revision_judgements = []
revision_texts = dict()

declined_files = list(declined_folder.iterdir())
accepted_files = list(accepted_folder.iterdir())

files_to_examine = declined_files + accepted_files
categories = ['declined'] * len(declined_files) + ['accepted'] * len(accepted_files)
zipped = list(zip(files_to_examine, categories))

for (filepath, folder) in tqdm(zipped):
    raw_xml = ET.parse(filepath)
    ns = {'mw': 'http://www.mediawiki.org/xml/export-0.10/'}
    page = raw_xml.find('mw:page', ns)
    pageid = page.find('mw:id', ns).text
    revisions = page.findall('mw:revision', ns)
    for revision in revisions:
        revid = revision.find('mw:id', ns).text
        edit_summary_elem = revision.find('mw:comment', ns)
        if edit_summary_elem is not None and edit_summary_elem.text is not None:
            edit_summary = edit_summary_elem.text
            judgement = None
            if 'Publishing accepted' in edit_summary:
                judgement = True
            elif 'Declining submission' in edit_summary:
                judgement = False
            elif 'Rejecting submission' in edit_summary:
                judgement = False
            else:
                continue
            revision_judgements.append({
                'folder': folder,
                'pageid': pageid,
                'revid': revid,
                'judgement': judgement
            })
            revision_texts[revid] = revision.find('mw:text', ns).text
pd.DataFrame(revision_judgements).to_csv('../../data/interim/annotated-judgements.csv')
with open('../../data/interim/revision-texts.json', 'w') as f:
    json.dump(revision_texts, f)