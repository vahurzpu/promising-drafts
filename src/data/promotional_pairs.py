import requests
from tqdm import tqdm
import json
import mwparserfromhell as mwp
from mwparserfromhell.wikicode import Wikicode
from typing import List, Tuple

def is_node_normal(node):
    return any((
        type(node) == mwp.wikicode.Text,
        type(node) == mwp.wikicode.Wikilink and ':' not in str(node.title),
        type(node) == mwp.wikicode.Tag and str(node.tag) in {'b', 'i', 'em', 'strong'}
    ))

def textify(node):
    if type(node) == mwp.wikicode.Text:
        return str(node)
    elif type(node) == mwp.wikicode.Wikilink:
        return str(node.title)
    elif type(node) == mwp.wikicode.Tag:
        return str(node.contents)

def get_body_paragraphs(content: str) -> Tuple[List[Wikicode], List[str]]:
    paras = content.split('\n\n')
    body_paras_code = []
    body_paras_text = []
    for para in paras:
        text = ''.join([textify(node) for node in mwp.parse(para).nodes if is_node_normal(node)]).strip()
        if text != '':
            body_paras_code.append(para)
            body_paras_text.append(text)
    return body_paras_code, body_paras_text

raw_query_results = requests.get('https://quarry.wmcloud.org/run/595237/output/0/json').json()


def revision_wikitext(revid) -> str:
    return requests.get('https://en.wikipedia.org/w/api.php', params={
        'action': 'query',
        'prop': 'revisions',
        'revids': revid,
        'rvslots': '*',
        'rvprop': 'content',
        'formatversion': 2,
        'format': 'json'
    }, headers={
        'User-Agent': 'DraftAcceptPrediction/0.0 (by User:Vahurzpu; mmwootten@outlook.com)'
    }).json()['query']['pages'][0]['revisions'][0]['slots']['main']['content']

def clean_wikitext(wikitext: str) -> str:
    body_paras_code, body_paras_text = get_body_paragraphs(wikitext)
    return '\n\n'.join(body_paras_text)

def assemble_record(title: str, revid, is_tagged: bool) -> dict:
    raw_wikitext = revision_wikitext(revid)
    cleaned_text = clean_wikitext(raw_wikitext)
    return {
        'page': title,
        'revid': revid,
        'tagged_promo': is_tagged,
        'raw_contents': raw_wikitext,
        'clean_contents': cleaned_text
    }

records = []
for (ts, title, oldid_without_tl, oldid_with_tl, actor) in tqdm(raw_query_results['rows']):
    try:
        old_record = assemble_record(title, oldid_with_tl, True)
        new_record = assemble_record(title, oldid_without_tl, False)
        if ('#redirect' in old_record['raw_contents'].lower()) or ('#redirect' in new_record['raw_contents'].lower()):
            continue
        if old_record['clean_contents'].strip() == '' or new_record['clean_contents'].strip() == '':
            continue
        records.append(old_record)
        records.append(new_record)
    except:
        pass

with open('../../data/raw/promo-tagged-pairs.json', 'w') as f:
    json.dump(records, f)