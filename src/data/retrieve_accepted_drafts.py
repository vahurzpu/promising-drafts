import requests
import re
import pywikibot
from pathlib import Path
from xml.etree import ElementTree as ET
from tqdm import tqdm

raw_query_results = requests.get('https://quarry.wmcloud.org/run/588744/output/0/json').json()
EDIT_SUMMARY_PATTERN = re.compile('Adding \[\[(.*)\]\] to list of recent AfC creations .*')
USER_AGENT = "DraftAcceptPrediction/0.0 (by User:Vahurzpu; mmwootten@outlook.com)"
enwiki = pywikibot.Site('en', 'wikipedia')

skipped = []
for (timestamp, edit_summary) in tqdm(raw_query_results['rows']):
    match = re.fullmatch(EDIT_SUMMARY_PATTERN, edit_summary)
    if not match:
        continue
    article = pywikibot.Page(enwiki, match.group(1))

    page_xml_file = Path(f'../../data/raw/accepted-drafts/{article.pageid}.xml')
    if page_xml_file.exists():
        continue

    try:
        exported_xml = requests.post('https://en.wikipedia.org/wiki/Special:Export', params={
            'pages': article.title(),
            'dir': 'desc',
            'offset': timestamp
        }, headers={
            'User-Agent': USER_AGENT
        }).text
        ns = {'mw': 'http://www.mediawiki.org/xml/export-0.10/'}
        page_id = int(ET.fromstring(exported_xml).find('mw:page', ns).find('mw:id', ns).text)
        assert page_id == article.pageid
        with open(page_xml_file, 'w') as f:
            f.write(exported_xml)
    except Exception as e:
        skipped.append(match.group(1))
print(skipped)