import pandas as pd
import requests
import json
from tqdm import tqdm

annotated_judgements = pd.read_csv('../../data/interim/annotated-judgements.csv')
annotated_judgements = annotated_judgements.sample(frac=1).reset_index(drop=True)

complete_revids = set()
with open('../../data/interim/ores-scores.json', 'r') as f:
    for line in f:
        complete_revids.add(json.loads(line)['revid'])

with open('../../data/interim/ores-scores.json', 'w') as f:
    for (_, judgment) in tqdm(list(annotated_judgements.iterrows())):
        revid = judgment['revid']
        if revid not in complete_revids:
            request_url = f'https://ores.wikimedia.org/v3/scores/enwiki/{revid}/articlequality'
            ores_response = requests.get(request_url, headers={
                'User-Agent': 'DraftAcceptPrediction/0.0 (by User:Vahurzpu; mmwootten@outlook.com)'
            }).json()
            model_response = next(iter(ores_response['enwiki']['scores'].values()))['articlequality']
            if 'error' in model_response:
                print(json.dumps({'revid': revid, 'error': model_response['error']['message']}), file=f)
            else:
                scores = next(iter(ores_response['enwiki']['scores'].values()))['articlequality']['score']['probability']
                scores['revid'] = judgment['revid']
                print(json.dumps(scores), file=f)