import json
import random

with open('../../data/raw/promo-tagged-pairs.json', 'r') as f:
    records = json.load(f)
lines = []

for record in records:
    line = '__label__'
    line +=  'promotional ' if record['tagged_promo'] else 'nonpromotional '
    line += record['clean_contents'].replace('\n', ' ')
    lines.append(line)
random.shuffle(lines)

cut1 = int(0.7 * len(lines))
cut2 = int(0.9 * len(lines))

def write_lines(name, lines):
    with open(f'../../data/interim/promo-tags/{name}.txt', 'w') as f:
        f.write('\n'.join(lines))

write_lines('train', lines[:cut1])
write_lines('test', lines[cut1:cut2])
write_lines('dev', lines[cut2:])